package orderBookSimulator;

import static orderBookSimulator.Utils.*;

import java.util.LinkedHashMap;

/**
 * Implements the matching algorithm.
 */
public final class Matcher {

    private Matcher() {
        privateConstructor(getClass());
    }

    /**
     * Matches the {@code newOrder} with orders on the book. Does not insert
     * the remainder of {@code newOrder}.
     *
     * @param book
     *         opposite side of the book
     * @param newOrder
     *         new order to match against
     *
     * @return any trades in the price, time priority, appropriately merged
     *
     * @throws NullPointerException
     *         if any argument is null
     * @throws IllegalArgumentException
     *         if the order's and the book's sides are not opposite
     */
    public static Iterable<Trade> match(final OrderBook.BookSide book,
                                        final Order newOrder) {
        notNull(book, "book");
        notNull(newOrder, "newOrder");
        checkArgument(book.getSide().opposite().equals(newOrder.getSide()),
                      "order must be opposite to the book side");


        final LinkedHashMap<Types.TradeKey, Trade> trades = new
                LinkedHashMap<>();

        Order topOfBook;

        while (null != (topOfBook = book.topOfBook()) && newOrder.isOpen()) {

            // Try to match
            final Trade maybeTrade = tryMatch(topOfBook, newOrder);
            if (null == maybeTrade) {
                break;
            }

            // Add the trade
            final Trade existingTrade = trades.get(maybeTrade.getTradeKey());
            if (null != existingTrade) {
                trades.put(existingTrade.getTradeKey(), mergeQuantities
                        (existingTrade, maybeTrade));
            } else {
                trades.put(maybeTrade.getTradeKey(), maybeTrade);
            }

            // Remove the topOfBook if filled
            if (!topOfBook.isOpen()) {
                book.removeTopOfBook();
            }
        }

        return trades.values();
    }

    /**
     * Tries to match the two orders. If a match occurs the quantities of the
     * two orders will be adjusted by the trade quantity.
     *
     * @param resting
     *         resting order
     * @param newOrder
     *         new order
     *
     * @return trade if they match, null otherwise
     *
     * @throws NullPointerException
     *         if any argument is null
     * @throws IllegalArgumentException
     *         if {@code resting} order is not the {@code opposite} side to
     *         newOrder or either order is filled or any order is already
     *         filled
     */
    private static Trade tryMatch(final Order resting,
                                  final Order newOrder) {

        notNull(resting, "resting");
        notNull(newOrder, "newOrder");
        checkArgument(resting.isOpposite(newOrder),
                      "resting.side.opposite != newOrder.side");
        checkArgument(resting.isOpen(), "resting order filled");
        checkArgument(newOrder.isOpen(), "newOrder order filled");

        // Check for matching prices
        final boolean isTrade;
        if (newOrder.getSide().isBuy()) {
            isTrade = newOrder.getPrice().compareTo(resting.getPrice()) >= 0;
        } else {
            isTrade = newOrder.getPrice().compareTo(resting.getPrice()) <= 0;
        }

        if (isTrade) {

            // Quantity: min of the remaining quantities
            final Types.Quantity tradeQuantity = resting.getRemainingQty()
                    .min(newOrder.getRemainingQty());

            // Price: always that of the resting order
            final Types.Price tradePrice = resting.getPrice();

            // Execute both orders: by definition this is correct
            resting.execute(tradeQuantity);
            newOrder.execute(tradeQuantity);

            // Create the trade
            return Trade.newTrade(resting, newOrder, tradePrice, tradeQuantity);
        }

        return null;
    }

    private static Trade mergeQuantities(final Trade trade1, final Trade
            trade2) {
        notNull(trade1, "trade1");
        notNull(trade1, "trade1");
        checkArgument(trade1.getTradeKey().equals(trade2.getTradeKey()),
                      "tradeKeys not equal");

        final Types.TradeKey tradeKey = trade1.getTradeKey();
        final Types.Quantity mergedQty = trade1.getQuantity().plus(trade2.getQuantity());

        return Trade.newTrade(tradeKey, mergedQty);
    }

}
