package orderBookSimulator;

import static orderBookSimulator.Utils.notNull;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import orderBookSimulator.Types.Side;
/**
 * Main entry point to the order book simulator.
 */
public final class Main {
    /**
     * Main entry point to the order book simulator.
     *
     * @param args
     *         no arguments are expected.
     */
    public static void main(final String... args) throws Exception {
        if (0 != args.length) {
            System.err.println("No arguments please. Exiting...");
            System.exit(-1);
        }
        int numOrders = 30;
        int sizeMean = 100000;
        int sizeStd = 20000;
        int pxMean = 50;
        int pxStd = 6;
        Random rand = new Random(); 
        ArrayList<Order> orderList=new ArrayList<Order>();  
        for (int i=0; i<numOrders; i++) {
        	int size = (int) (rand.nextGaussian()*sizeStd + sizeMean);
        	short priceInt = (short) (rand.nextGaussian()*pxStd + pxMean);
        	int sideNum = ThreadLocalRandom.current().nextInt(1, 3);
        	Side side = Side.BUY;
        	if (sideNum ==1) {
                side= side.opposite();}
                final Types.OrderId orderId = Types.orderId(i+1);
                final Types.Price price = Types.price(priceInt);
                final Types.Quantity quantity = Types.quantity(size);
                Order newOrder = new Order(side, orderId, price, quantity);
        		orderList.add(newOrder);
        }
        for (int i=0; i<numOrders; i++) {
        	System.out.print(orderList.get(i).toString());
            System.out.println();
        }

        final OrderBook book = OrderBook.emptyBook();

        for (int i=0; i<numOrders; i++) {
        
            final Order newOrder = orderList.get(i);
            // Lookup the opposite book
            final OrderBook.BookSide opposite
                    = book.getBookSide(newOrder.getSide().opposite());

            // Do the matching and get the trades
            final Iterable<Trade> trades = Matcher.match(opposite, newOrder);

            // Put the remainder of the newOrder into the book
            if (newOrder.isOpen()) {
                final OrderBook.BookSide sameSide
                        = book.getBookSide(newOrder.getSide());

                sameSide.insert(newOrder);
            }
            final PrintWriter out1 = new PrintWriter(System.out, true);
            
	        final OutputFormatter f1 = new OutputFormatter(notNull(out1, "out1"));
            // Print trades
            for (final Trade trade : trades) {
                f1.append(trade);
            }
	
            // Print the book
            f1.append(book);

            // Flush
            f1.flush();
            }

    }
}
