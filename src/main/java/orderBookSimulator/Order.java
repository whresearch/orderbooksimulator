package orderBookSimulator;

import static orderBookSimulator.Utils.notNull;


public final class Order {

    private final Types.Side side;
    private final Types.OrderId orderId;
    private final Types.Price price;
    private Types.Quantity remainingQty;

    /**
     * Default constructor.
     *
     * @param side
     *         side of the order
     * @param orderId
     *         id of the order
     * @param price
     *         price of the order
     * @param quantity
     *         original quantity of the order
     * @throws NullPointerException
     *         if any argument is null
     */
    public Order(final Types.Side side, final Types.OrderId orderId,
                 final Types.Price price, final Types.Quantity quantity) {
        notNull(side, "side");
        notNull(orderId, "orderId");
        notNull(price, "price");
        notNull(quantity, "quantity");
        this.side = side;
        this.orderId = orderId;
        this.price = price;

        this.remainingQty = quantity;
    }

    public Types.Side getSide() {
        return side;
    }

    public Types.OrderId getOrderId() {
        return orderId;
    }

    public Types.Price getPrice() {
        return price;
    }
    
    public Types.Quantity getRemainingQty() {
        return remainingQty;
    }
    
    public boolean isOpen() {
        return !remainingQty.isZero();
    }

    public boolean isOpposite(final Order other) {
        notNull(other, "other");
        return side.opposite().equals(other.getSide());
    }


    public void execute(final Types.Quantity toExecute) {
        notNull(toExecute, "toExecute cannot be null");
        remainingQty = remainingQty.minus(toExecute);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Order order = (Order) o;

        if (!orderId.equals(order.orderId)) {
            return false;
        }

        if (!price.equals(order.price)) {
            return false;
        }
        if (!remainingQty.equals(order.remainingQty)) {
            return false;
        }
        if (side != order.side) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = side.hashCode();
        result = 31 * result + orderId.hashCode();
        result = 31 * result + price.hashCode();
        result = 31 * result + remainingQty.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "side=" + side +
                ", orderId=" + orderId +
                ", price=" + price +
                ", quantity=" + remainingQty +
                '}';
    }

    public static Order newLimitOrder(final Types.Side side,
                                      final Types.OrderId orderId,
                                      final Types.Price price,
                                      final Types.Quantity quantity) {
        return new Order(side, orderId, price, quantity);
    }
}
