package orderBookSimulatorTest;

import static org.junit.Assert.*;

import org.junit.Test;

import orderBookSimulator.Matcher;
import orderBookSimulator.Order;
import orderBookSimulator.OrderBook;
import orderBookSimulator.Trade;
import orderBookSimulator.Types;
import orderBookSimulator.Types.*;



public class MatcherTest {

	@Test
	public void testMatcher() {
		final OrderBook book = OrderBook.emptyBook();
    	Side side1 = Side.BUY;

        final Types.OrderId orderId1 = Types.orderId(1);
        final Types.Price price1 = Types.price((short) 99);
        final Types.Quantity quantity1 = Types.quantity(5);
		Order order1 = new Order(side1, orderId1, price1, quantity1);
        final OrderBook.BookSide opposite1 = book.getBookSide(order1.getSide().opposite());
        // Do the matching and get the trades
        final Iterable<Trade> trades1 = Matcher.match(opposite1, order1);
        if (order1.isOpen()){
        	final OrderBook.BookSide sameSide = book.getBookSide(order1.getSide());
        	sameSide.insert(order1);
        	}
        
		assertEquals(trades1.toString(),"[]");
		
		Side side2 = Side.SELL;
        final Types.OrderId orderId2 = Types.orderId(2);
        final Types.Price price2 = Types.price((short) 99);
        final Types.Quantity quantity2 = Types.quantity(5);
		Order order2 = new Order(side2, orderId2, price2, quantity2);
        final OrderBook.BookSide opposite2
        = book.getBookSide(order2.getSide().opposite());
        
        final Iterable<Trade> trades2 = Matcher.match(opposite2, order2);
        if (order2.isOpen()){
        	final OrderBook.BookSide sameSide2 = book.getBookSide(order2.getSide());
        	sameSide2.insert(order2);
        }
        
        assertEquals(trades2.toString(), "[Trade{tradeKey=TradeKey{buyOrderId=OrderId{orderId=1}, "
                						+ "sellOrderId=OrderId{orderId=2}, price=Price{price=99}}, "
                						+ "quantity=Quantity{quantity=5}}]");

	}

}
